import os.path
import matplotlib as mpl
from datetime import datetime, timedelta
mpl.rcParams['agg.path.chunksize'] = 10000  # 或者更高的值
from lspopt import spectrogram_lspopt
import matplotlib.dates as mdates
from  transdata2edf import *
import matplotlib.pyplot as plt
from hb import *
import numpy as np

def rawdataPlot(EEG_TOI, sf, ax, start_times, savepath, title='EEG', figuresize=(16, 4.5)):
    # 设置轴对象的图形大小
    ax.figure.set_size_inches(figuresize[0], figuresize[1])

    t = np.arange(0, len(EEG_TOI)) / sf
    t_datetime = [start_times + timedelta(seconds=x) for x in t]  # 将秒转换为日期时间格式

    ax.plot(t_datetime, EEG_TOI/1000, linewidth=0.5, color='black')

    # 设置x轴标签角度
    plt.setp(ax.get_xticklabels(), rotation=45, ha='right')
    ax.set_xlim([min(t_datetime), max(t_datetime)])
    ax.set_ylim([-0.3, 0.3])
    ax.set_title(title)
    ax.set_ylabel('Voltage(mV)')
    #ax.set_xlabel('Time')

    # 设置X轴标签格式为年-月-日 小时:分钟:秒
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
    ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=60))
    plt.tight_layout()
    png_path = os.path.join(savepath, title+"_rawdata.png")
    plt.savefig(png_path)
    plt.close()
    print('Raw data plot finished!')

def spectrogramPlot(data,sf,St2TOITime,TimeRange, title,savepath,fmin=0.5, fmax=35, cmap='RdBu_r', colorbar_percentile=10, time_resolution_sec=0.5,figuresize= (19, 4.5)):
    nperseg = int(time_resolution_sec * sf)  # window length based on time resolution
    f, t, Sxx = spectrogram_lspopt(data, sf, nperseg=nperseg, noverlap=nperseg // 2)
    Sxx = 10 * np.log10(Sxx)  # Convert to dB
    good_freqs = np.logical_and(f >= fmin, f <= fmax)
    Sxx = Sxx[good_freqs, :]
    f = f[good_freqs]
    # t =np.round(t,1)
    # Calculate colorbar limits based on percentile
    Sxx_flat = Sxx.flatten()
    vmin, vmax = np.percentile(Sxx_flat, [colorbar_percentile, 100 - colorbar_percentile])
    fig, ax = plt.subplots(figsize=figuresize)  # 增加图表尺寸
    plt.subplots_adjust(bottom=0.2)  # 调整底部空间
    # Plotting
    im=ax.pcolormesh(t, f, Sxx, cmap=cmap, shading='gouraud', vmin=vmin, vmax=vmax)
    ax.set_ylabel('Frequency [Hz]')
    # ax.set_xlabel('Time(s)')
    # ax.set_xlim(t[0], t[-1])
    # xticks=(t[::15]).tolist()
    # ax.set_xticks(xticks)
    # xticklabels=(np.array(xticks).astype('int16')-TimeRange).tolist()
    # ax.set_xticklabels(xticklabels, rotation=30, fontsize='small')
    ax.set_title(title)
    ax.set_xticks([])
    # 设置x轴标签角度
    # plt.setp(ax.get_xticklabels(), rotation=45, ha='right')
    # Setting the x-axis formatter to display date and time q
    plt.tight_layout()
    ax.legend(loc='upper right')
    # plt.show()
    png_path = os.path.join(savepath, "EEG_Spectrogram.png")
    plt.savefig(png_path)
    svg_path = os.path.join(savepath,"EEG_Spectrogram.svg")
    plt.savefig(svg_path, dpi=600)
    plt.close(fig)
    print('spectrum plot finished!')


    t = np.arange(0, len(data)) / sf
    plt.figure(figsize=figuresize)
    plt.plot(t, data, linewidth=0.5, color='black')
    plt.xlim(t[0], t[-1])
    xticks=(t[::30*sf]).tolist()
    plt.xlabel('Time(s)')
    plt.ylabel('Voltage(uV)')
    xticklabels=(np.array(xticks).astype('int16')-TimeRange).tolist()
    plt.xticks(xticks,xticklabels,rotation=45)
    plt.ylim([-600, 600])
    plt.title(title)
    plt.tight_layout()
    png_path = os.path.join(savepath, title+"Spectrogram_rawdata.png")
    plt.savefig(png_path)
    svg_path = os.path.join(savepath,title+"Spectrogram_rawdata.svg")
    plt.savefig(svg_path, dpi=300)
    plt.close()
    print('Raw data plot finished!')




def PSDPlot(raw,
            savepath,
            fmax = 50,# 最高频率
            figuresize=(16,4.5)):
    # 计算PSD
    raw.compute_psd()
    # 获取PSD数据和频率信息
    psd_data, freqs = raw.compute_psd().get_data(return_freqs=True)
    # 筛选频率数据，只保留小于50 Hz的部分
    freqs_filtered = freqs[freqs < 50]
    psd_data_filtered = psd_data[:, freqs < fmax]
    # 绘制PSD图
    plt.figure(figsize=figuresize)
    plt.semilogy(freqs_filtered, psd_data_filtered[0], color='black')  # 使用semilogy绘制双对数坐标图
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Power/Frequency (dB/Hz)')
    plt.title('Power Spectral Density(PSD)')
    plt.ylim([0,100000])
    plt.xlim([min(freqs_filtered)-5, max(freqs_filtered)])
    plt.legend()
    plt.grid(False)
    # plt.show()
    png_path = os.path.join(savepath, "EEG_PSD.png")
    plt.savefig(png_path)
    svg_path = os.path.join(savepath,"EEG_PSD.svg")
    plt.savefig(svg_path, dpi=300)
    plt.close()
    print('PSD plot finished!')

