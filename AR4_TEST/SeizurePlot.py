import os
import scipy.io as sio
import mne
import scipy.signal as signal
from datetime import datetime,timedelta
from Utils import *



# 数据加载及预处理
# dataPath=r'E:\AR4Data\HMX\20240113\85fb\14152'# 数据所在路径，数据格式应为.mat
# dataPath=r'E:\AR4Data\HMX\20240113\072f\14122'
# dataPath=r'D:\Data\AR4\HMX\Data\20240118\85fb\14531'
# dataPath=r'D:\Data\AR4\HMX\Data\20240118\22b7\14550'
dataPath=r'D:\Data\AR4\HMX\Data\20240118\072f\14551'
FigSavePath=os.path.join(dataPath,'Seizure'+"("+dataPath[-5:]+")")
if not os.path.exists(FigSavePath):
    os.makedirs(FigSavePath)
EEG_EMG_Raw_AR4 = sio.loadmat(os.path.join(dataPath, 'eeg.mat'))
EEG=EEG_EMG_Raw_AR4['EEG'][0,:]
EMG=EEG_EMG_Raw_AR4['EEG'][1,:]

start_time = EEG_EMG_Raw_AR4['StartTime'][0]
start_time = list(map(int, start_time))
sf_EEG = int(EEG_EMG_Raw_AR4['SampleRate'])
sf_ACC = 50

# 创建MNE的Raw对象
info = mne.create_info(ch_names=['signal'], sfreq=sf_EEG, ch_types=['eeg'])
raw = mne.io.RawArray(EEG[np.newaxis,:], info)


# 数据预处理
b_hp, a_hp = signal.butter(3, 1 / (sf_EEG / 2), 'highpass')
EEG = signal.lfilter(b_hp, a_hp, EEG)
b_lp, a_lp = signal.butter(3, 45 / (sf_EEG / 2), 'lowpass')
EEG = signal.lfilter(b_lp, a_lp, EEG)

b_hp, a_hp = signal.butter(3, 20 / (sf_EEG / 2), 'highpass')
EMG = signal.lfilter(b_hp, a_hp, EMG)
b_lp, a_lp = signal.butter(3, 100 / (sf_EEG / 2), 'lowpass')
EMG = signal.lfilter(b_lp, a_lp, EMG)


# 癫痫发作时间点设置
# TOI=[2024,1,12,19,53,20]# time of interest \14152
# TOI=[2024,1,13,16,10,30]# time of interest \14122
# TOI=[2024,1,18,13,58,20]# time of interest \14531
# TOI=[2024,1,18,14,3,20]# time of interest \14531
# TOI=[2024,1,18,13,52,45]# time of interest \14550
TOI=[2024,1,18,14,4,50]# time of interest \14550

St2TOITime = datetime(TOI[0],TOI[1],TOI[2],TOI[3],TOI[4],TOI[5])-datetime(start_time[0],start_time[1],start_time[2],start_time[3],start_time[4],start_time[5])
St2TOITime=int(St2TOITime.total_seconds())
EEG_TOI=EEG[St2TOITime*sf_EEG:(St2TOITime+30)*sf_EEG]
EMG_TOI=EMG[St2TOITime*sf_EEG:(St2TOITime+30)*sf_EEG]


#绘图
figuresize = (19, 4.5)
rawdataPlot(EEG_TOI,sf=sf_EEG,savepath=FigSavePath,title='EEG',figuresize=(16,4.5))# EEG原始数据绘制
rawdataPlot(EMG_TOI,sf=sf_EEG,savepath=FigSavePath,title='EMG',figuresize=(16,4.5))# EMG原始数据绘制
PSDPlot(raw,savepath=FigSavePath,fmax = 50,figuresize=(16,4.5))# PSD数据绘制

TimeRange=320
EEG_TOI=EEG[(St2TOITime-TimeRange)*sf_EEG:(St2TOITime+TimeRange)*sf_EEG]
spectrogramPlot(EEG_TOI,sf_EEG,St2TOITime,TimeRange,'EEG',savepath=FigSavePath,fmin=0.5, fmax=35, cmap='RdBu_r', colorbar_percentile=1, time_resolution_sec=5,figuresize= figuresize)# EEG 频谱分析图绘制


