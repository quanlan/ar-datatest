import qlelib
import time
import os
from decode_data import eegIsX8
qldata2edf = qlelib.qldata2edf
qle2edf = qlelib.qle2edf
repair = qlelib.qle_data_check
def find_file(root, func, recursion = True):
    ret = []
    for f in os.listdir(root):
        full_name = root + '/' + f
        if(os.path.isfile(full_name)):
            if func(full_name):
                ret.append(full_name)
        elif recursion:
            ret += find_file(full_name, func)
    return ret  


def data_translate_edf(data_p):
    qldata2edf.convert(data_p)

def data_translate_edf_path(data_p:str):
    def func(path:str):
        return path.endswith('.eeg') or path.endswith('eeg.qle')
    eeg_p = find_file(data_p, func, recursion=False)
    if len(eeg_p) == 1:
        eeg_p = eeg_p[0]
        if eegIsX8(eeg_p):
            acc_p = find_file(data_p, lambda _: _.endswith('.acc') or _.endswith('acc.qle'), recursion=False)
            tri_p = find_file(data_p, lambda _: _.endswith('tri.tri') or _.endswith('tri.dat'), recursion=False)
            sw_p = find_file(data_p, lambda _: _.endswith('sti.log'), recursion=False)
            dir_info = {'EEG':eeg_p}
            if len(acc_p) == 1:
                dir_info['ACC'] = acc_p[0]
            if len(tri_p) == 1:
                dir_info['TRI'] = tri_p[0]
            if len(sw_p) == 1:
                dir_info['SW'] = sw_p[0]
            qle2edf.convert_dir(dir_info, os.path.join(data_p, os.path.basename(data_p)))
            return
    def func(path:str):
        return path.endswith('.eeg') or path.endswith('.acc') or path.endswith('.qle')
    all_file = find_file(data_p, func, recursion=False)
    for f in all_file:
        qldata2edf.convert(f)

if __name__ == '__main__':
    # data_translate_edf_path(r'C:\Users\fengxinan\Downloads\9564')
    # repair.qle_data_check(r'C:\Users\fengxinan\Downloads\9543_acc.acc', r'C:\Users\fengxinan\Downloads\9543_racc.acc')
    data_translate_edf(r'C:\Users\fengxinan\Downloads\9539_1706948450050.eeg.eeg')