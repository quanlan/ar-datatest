class IChannelInfo:
    desc = {
                'label': None,
                'dimension': None,
                'sample_rate': None,
                'physical_max': None,
                'physical_min': None,
                'digital_max': None,
                'digital_min': None,
                'transducer': None,
                'prefilter': None
            }
    @staticmethod
    def gene_desc(label:str, dimension:str, sample_rate:int, physical_max:int, physical_min:int, digital_max:int, digital_min:int, transducer :str= '', prefilter:str = '') -> None:
        desc = {
                'label': label,
                'dimension': dimension,
                'sample_rate': sample_rate,
                'physical_max': physical_max,
                'physical_min': physical_min,
                'digital_max': digital_max,
                'digital_min': digital_min,
                'transducer': transducer,
                'prefilter': prefilter
                }
        return desc
    data = []
class IDataFormat:
    def __init__(self) -> None:
        self.start_time = 0
        self.channels = [] #type: list[IChannelInfo]

