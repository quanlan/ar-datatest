import os.path
import scipy.io as sio
import mne
import numpy as np
from scipy import signal
from matplotlib import pyplot as plt
import yasa
import matplotlib as mpl
from datetime import datetime,timedelta
mpl.rcParams['agg.path.chunksize'] = 10000  # 或者更高的值
import pandas as pd
from lspopt import spectrogram_lspopt
import matplotlib.dates as mdates
from data2mat import data_translate_mat
from  transdata2edf import *
import scipy as sp

import numpy as np



parent_directory_list =[
# r'D:\Data\AR4\DY_FuDAN\20240108_20240110',# 数据所在路径，精确到日期
# r'D:\Data\AR4\DY_FuDAN\20231220',
# r'D:\Data\AR4\DY_FuDAN\20231221',
# r'D:\Data\AR4\DY_FuDAN\20240119_20240121',
# r'D:\Data\AR4\DY_FuDAN\20240121_20240123',
# r'D:\Data\AR4\DY_FuDAN\20240123_20240125',
# r'D:\Data\AR4\万老师_湘雅医院\20240118',
r'D:\Data\X8\HLGHospital\20240301'
]

x_axis_interval = 5  # 横轴的时间间隔（min）
xaxis_interval_minutes=30
import os
def list_second_level_directory_paths(parent_directory):
    second_level_paths = []
    for folder in os.listdir(parent_directory):
        first_level_path = os.path.join(parent_directory, folder)
        if os.path.isdir(first_level_path):
            for subfolder in os.listdir(first_level_path):
                second_level_path = os.path.join(first_level_path, subfolder)
                if os.path.isdir(second_level_path):
                    second_level_paths.append(second_level_path)

    return second_level_paths

def EMGplot(EEG_TOI
            ,sf
            ,savepath
            ,title='EEG'
            ,window_size=5 #窗宽是数秒
            ,figuresize=(16,4.5)):
    def RmsCal(x, window_size):
        """
        Calculates the RMS of a signal with a sliding window.

        Args:
          x: The signal as a 1D numpy array.
          window_size: The size of the sliding window.

        Returns:
          rms: The RMS values of the signal with sliding window.
        """

        rms = np.sqrt(np.mean(np.square(np.convolve(x ** 2, np.ones(window_size) / window_size, mode='same')), axis=1))
        return rms

    plt.figure(figsize=figuresize)
    rms = RmsCal(EEG_TOI, window_size*sf)
    t = np.arange(0, (len(EEG_TOI)/window_size)) / (sf*window_size)
    plt.plot(t, rms/1000, linewidth=0.5, color='black')
    plt.xlim([min(t), max(t)])
    # plt.ylim([-0.3, 0.3])
    plt.title(title)
    plt.ylabel('Voltage(mV)')
    plt.xlabel('Time(s)')

    # plt.show()
    plt.tight_layout()
    png_path = os.path.join(savepath, title+"_EMG(RMS).png")
    plt.savefig(png_path)
    # svg_path = os.path.join(savepath,title+"_rawdata.svg")
    # plt.savefig(svg_path, dpi=300)
    plt.close()
    print('EMG(RMS) plot finished!')


# Function to plot the spectrogram
def plot_spectrogram(data, sf, ax, start_time_TF, title, fmin=0.5, fmax=35, cmap='RdBu_r', colorbar_percentile=1, xaxis_interval_minutes=xaxis_interval_minutes, time_resolution_sec=30):
    nperseg = int(time_resolution_sec * sf)  # window length based on time resolution
    f, t, Sxx = spectrogram_lspopt(data, sf, nperseg=nperseg, noverlap=nperseg // 2)
    Sxx = 10 * np.log10(Sxx)  # Convert to dB
    good_freqs = np.logical_and(f >= fmin, f <= fmax)
    Sxx = Sxx[good_freqs, :]
    f = f[good_freqs]

    # Calculate colorbar limits based on percentile
    Sxx_flat = Sxx.flatten()
    vmin, vmax = np.percentile(Sxx_flat, [colorbar_percentile, 100 - colorbar_percentile])

    # Convert time axis to datetime objects starting from start_time_TF
    times = [start_time_TF + timedelta(seconds=s) for s in t]

    # Plotting
    im = ax.pcolormesh(times, f, Sxx, cmap=cmap, shading='gouraud', vmin=vmin, vmax=vmax)
    ax.set_ylabel('Frequency [Hz]')
    ax.set_xlim(times[0], times[-1])
    ax.set_title(title)

    # Format the x-axis to display time correctly
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
    ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=xaxis_interval_minutes))  # Set x-axis interval
    ax.xaxis_date()  # Set x-axis to use date format

    # 设置x轴标签角度
    plt.setp(ax.get_xticklabels(), rotation=45, ha='right')
    # Setting the x-axis formatter to display date and time
    # Mark the start time point on the x-axis
    ax.axvline(x=times[0], color='green', linestyle='--', label='Start Time: ' + times[0].strftime('%Y-%m-%d %H:%M:%S'))
    plt.tight_layout()
    ax.legend(loc='upper right')
    return im

def data_translate_edf_path(data_p:str):
    def func(path:str):
        return path.endswith('.eeg') or path.endswith('eeg.qle')
    eeg_p = find_file(data_p, func, recursion=False)
    if len(eeg_p) == 1:
        eeg_p = eeg_p[0]
        if eegIsX8(eeg_p):
            acc_p = find_file(data_p, lambda _: _.endswith('.acc') or _.endswith('acc.qle'), recursion=False)
            tri_p = find_file(data_p, lambda _: _.endswith('tri.tri') or _.endswith('tri.dat'), recursion=False)
            sw_p = find_file(data_p, lambda _: _.endswith('sti.log'), recursion=False)
            dir_info = {'EEG':eeg_p}
            if len(acc_p) == 1:
                dir_info['ACC'] = acc_p[0]
            if len(tri_p) == 1:
                dir_info['TRI'] = tri_p[0]
            if len(sw_p) == 1:
                dir_info['SW'] = sw_p[0]
            qle2edf.convert_dir(dir_info, os.path.join(data_p, os.path.basename(data_p)))
            return
    def func(path:str):
        return path.endswith('.eeg') or path.endswith('.acc') or path.endswith('.qle')
    all_file = find_file(data_p, func, recursion=False)
    for f in all_file:
        qldata2edf.convert(f)




# 使用示例
for parent_directory in parent_directory_list:
    second_level_dirs = list_second_level_directory_paths(parent_directory)
    print(parent_directory)
    for file_path in second_level_dirs:
        file_path = r'D:\Data\X8\TJAne\SWSenhancement\20240530\2e47\21582'
        resultpath = file_path

        print(file_path)

        if not os.path.exists(os.path.join(file_path,'eeg.mat')):
            print("文件存在")
            data_translate_mat(os.path.join(file_path,'eeg.eeg'))
            print('translate',file_path,'eeg.eeg')

        if not os.path.exists(os.path.join(file_path, 'acc.mat')):
            data_translate_mat(os.path.join(file_path, 'acc.acc'))
            print('translate', file_path, 'acc.acc')

        # if not os.path.exists(os.path.join(file_path, file_path[-5::]+'EEG_EMG_acc_trigger.edf')):
        #     data_translate_edf_path(os.path.join(file_path, file_path[-5::]+'EEGEMGacctrigger.edf'))
        #     print('translate', file_path, 'EEG_EMG_acc_trigger.edf')

        # EEG EMG ACC load
        EEG_EMG_Raw_AR4 = sio.loadmat(os.path.join(file_path,'eeg.mat'))

        start_time=EEG_EMG_Raw_AR4['StartTime'][0]
        start_time = list(map(int, start_time))
        sf_EEG=int(EEG_EMG_Raw_AR4['SampleRate'])
        sf_ACC=50
        # st=int(0.3*60*60) #截去开头st秒数据
        st=0
        # et=int(50*60*60) #截去结束前et秒数据
        #47.5 65
        et = 0

        EEG_EMG=EEG_EMG_Raw_AR4['EEG']# 脑电和肌电数据
        ACC=sio.loadmat(os.path.join(file_path,'acc.mat'))['ACC']#体动数据
        threshhold=100
        b_hp, a_hp = signal.butter(3, 1 / (sf_EEG / 2), 'highpass')
        EEG_EMG[0, :] = signal.lfilter(b_hp, a_hp, EEG_EMG[0,:])
        b_lp, a_lp = signal.butter(3, 45 / (sf_EEG / 2), 'lowpass')
        EEG_EMG[0, :] = signal.lfilter(b_lp, a_lp, EEG_EMG[0,:])
        # EEG_EMG[0,:][abs(EEG_EMG[0,:])>threshhold]=threshhold

        b_hp, a_hp = signal.butter(3, 1 / (sf_EEG / 2), 'highpass')
        EEG_EMG[1, :] = signal.lfilter(b_hp, a_hp, EEG_EMG[1,:])
        b_lp, a_lp = signal.butter(3, 45 / (sf_EEG / 2), 'lowpass')
        EEG_EMG[1, :] = signal.lfilter(b_lp, a_lp, EEG_EMG[1,:])
        # EEG_EMG[1,:][abs(EEG_EMG[1,:])>threshhold]=threshhold

        # plt.plot(EEG_EMG[1, :])
        # plt.show()
        # print()




        EEG= EEG_EMG[0,st*sf_EEG:len(EEG_EMG[0,:]) - et * sf_EEG]
        EMG=EEG_EMG[1,st*sf_EEG:len(EEG_EMG[1,:]) - et * sf_EEG]
        ACC=np.squeeze(ACC)
        ACC=ACC[st*sf_ACC:len(ACC)-et*sf_ACC]
        start_time = datetime(start_time[0], start_time[1], start_time[2], start_time[3], start_time[4], start_time[5])+timedelta(seconds=st)


        #
        # plt.plot(EEG)
        # plt.show()


        figuresize = (19, 4.5)

        # Plotting EEG Spectrogram
        # 调整图表尺寸和布局
        if not os.path.exists(os.path.join(file_path,'EEG_Spectrogram.png')):

            fig, ax = plt.subplots(figsize=figuresize)  # 增加图表尺寸
            plt.subplots_adjust(bottom=0.2)  # 调整底部空间


            plot_spectrogram(EEG, sf_EEG, ax, start_time, "EEG Spectrogram",fmin=0,fmax=200, xaxis_interval_minutes=x_axis_interval)
            eeg_fig_path = os.path.join(file_path,"EEG_Spectrogram.png")
            # eeg_fig_path = os.path.join(file_path,"EEG_Spectrogram.svg")
            plt.savefig(eeg_fig_path)
            # plt.show()
            plt.close(fig)

        if not os.path.exists(os.path.join(file_path,'EMG_Spectrogram.png')):
            # Plotting EMG Spectrogram
            # 调整图表尺寸和布局
            fig, ax = plt.subplots(figsize=figuresize)  # 增加图表尺寸
            plt.subplots_adjust(bottom=0.2)  # 调整底部空间
            plot_spectrogram(EMG, sf_EEG, ax, start_time, "EMG Spectrogram",fmin=0,fmax=200, xaxis_interval_minutes=x_axis_interval)
            emg_fig_path = os.path.join(file_path,"EMG_Spectrogram.png")
            # emg_fig_path = os.path.join(file_path,"EMG_Spectrogram.svg")
            plt.savefig(emg_fig_path)
            plt.close(fig)

        if not os.path.exists(os.path.join(file_path,'ACC_corrected.png')):
            sampling_rate = 50  # 50Hz
            total_seconds = len(ACC) / sampling_rate
            times = pd.date_range(start=start_time, periods=len(ACC), freq='20ms')  #
            # Plotting
            fig, ax = plt.subplots(figsize=figuresize)
            plt.subplots_adjust(bottom=0.2)
            ax.plot(times, np.squeeze(ACC/100))
            ax.set_xlabel('Time')
            ax.set_ylabel('Acceleration')
            ax.set_title('ACC Data')
            # 设置x轴标签角度
            plt.setp(ax.get_xticklabels(), rotation=45, ha='right')
            # Setting the x-axis formatter to display date and time
            plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d %H:%M:%S'))
            # Adjusting layout to prevent clipping of date labels

            ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=x_axis_interval))  # Set x-axis interval to every 10 seconds
            ax.xaxis_date()
            ax.set_xlim(times[0], times[-1])
            plt.tight_layout()
            acc_fig_path = os.path.join(file_path, "ACC_corrected.png")
            plt.savefig(acc_fig_path)
            plt.close(fig)
            print('finish')

        # if not os.path.exists(os.path.join(file_path, 'EMG_plot.png')):
        #     # Plotting EMG Spectrogram
        #     # 调整图表尺寸和布局
        #     # fig, ax = plt.subplots(figsize=figuresize)  # 增加图表尺寸
        #     # plt.subplots_adjust(bottom=0.2)  # 调整底部空间
        #     # plot_spectrogram(EMG, sf_EEG,ax, start_time, "EMG Spectrogram",fmin=20,fmax=100, xaxis_interval_minutes=x_axis_interval)
        #     EMGplot(EMG, sf=sf_EEG, savepath=file_path, title='EMG',window_size=5, figuresize=figuresize)  # EMG原始数据绘制
