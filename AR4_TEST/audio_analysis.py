
import os
import numpy as np
import matplotlib.pyplot as plt
import librosa
import librosa.display
from scipy.signal import welch
from lspopt import spectrogram_lspopt

import numpy as np
from scipy.io.wavfile import write

# 设置信号参数
sample_rate = 44100  # 采样率，通常为44.1kHz
duration = 5  # 持续时间，单位为秒
frequency = 40  # 信号频率，单位为Hz

# 生成时间轴
t = np.linspace(0, duration, int(sample_rate * duration), endpoint=False)

# 生成40Hz的正弦波信号
signal = 0.5 * np.sin(2 * np.pi * frequency * t)  # 振幅设置为0.5以避免失真

# 保存为WAV文件
write(r'D:\Data\X8\TJAne\20240626\audio1\40Hz_signal.wav', sample_rate, signal.astype(np.float32))

print("40Hz信号的音频已生成并保存为40Hz_signal.wav")



# 加载音频文件
# audio_path = r'D:\Data\X8\TJAne\20240626\audio1\促进脑神经（40 Hz）.wav'
audio_path = r'D:\Data\X8\TJAne\20240626\audio1\40Hz_signal.wav'
data, sr = librosa.load(audio_path, sr=None)

plt.plot(data)
plt.show()

# time frequency
time_resolution_sec = 2
fmin = 0
fmax = 200
nperseg = int(time_resolution_sec * sr)  # window length based on time resolution
f, t, Sxx = spectrogram_lspopt(data, sr, nperseg=nperseg,
                               noverlap=nperseg // 2)

Sxx = 10 * np.log10(Sxx)

good_freqs = np.logical_and(f >= fmin, f <= fmax)
Sxx = Sxx[good_freqs, :]
f = f[good_freqs]
vmin = np.percentile(Sxx, 1)
vmax = np.percentile(Sxx, 99)
# vmin = -1
# vmax = 28
plt.figure(figsize=(19, 4.5))
plt.pcolormesh(t, f, Sxx, shading='gouraud', cmap='RdBu_r', vmin=vmin, vmax=vmax)
plt.ylabel('Frequency (Hz)')
plt.xlabel('Time (s)')
plt.title(f'Time-Frequency')
plt.ylim([0, 200])
plt.colorbar(label='Power Spectral Density (dB)')
plt.tight_layout()
plt.xlim([min(t), max(t)])

plt.savefig(os.path.join(os.path.dirname(audio_path),
                         f'timefrequency.png'))
plt.close()

# PSD
fmin = 0.5
fmax = 200
plt.figure(figsize=(9, 6))
freqs, psd = welch(data, sr, nperseg= int(time_resolution_sec * sr))
psd[psd <= 0] = np.nan
psd = 10 * np.log10(psd)
freqs_filtered = freqs[(freqs >= fmin) & (freqs <= fmax)]
psd_filtered = psd[(freqs >= fmin) & (freqs <= fmax)]
plt.plot(freqs_filtered, psd_filtered, color='black')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Power Spectral Density (dB/Hz)')
plt.tight_layout()
plt.savefig(os.path.join(os.path.dirname(audio_path),
                         f'PSD.png'))
plt.close()



