

import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

def highpass_filter(EEG, cutoff_frequency, fs, order=3):
    nyquist = 0.5 * fs
    normalized_cutoff = cutoff_frequency / nyquist
    b_hp, a_hp = signal.butter(order, normalized_cutoff, btype='highpass')
    EEG_filtered = signal.lfilter(b_hp, a_hp, EEG)
    return EEG_filtered

def lowpass_filter(EEG, cutoff_frequency, fs, order=3):
    nyquist = 0.5 * fs
    normalized_cutoff = cutoff_frequency / nyquist
    b_lp, a_lp = signal.butter(order, normalized_cutoff, btype='lowpass')
    EEG_filtered = signal.lfilter(b_lp, a_lp, EEG)
    return EEG_filtered

def bandpass_filter(EEG, low_cutoff_frequency, high_cutoff_frequency, fs, order=3):
    nyquist = 0.5 * fs
    low_cutoff = low_cutoff_frequency / nyquist
    high_cutoff = high_cutoff_frequency / nyquist
    b_bp, a_bp = signal.butter(order, [low_cutoff, high_cutoff], btype='bandpass')
    EEG_filtered = signal.lfilter(b_bp, a_bp, EEG)
    return EEG_filtered

def notch_filter(EEG, low_cutoff_frequency, high_cutoff_frequency, fs, order=3):
    nyquist = 0.5 * fs
    low_cutoff = low_cutoff_frequency / nyquist
    high_cutoff = high_cutoff_frequency / nyquist
    b, a = signal.butter(order, [low_cutoff, high_cutoff], btype='bandstop')
    EEG_filtered = signal.filtfilt(b, a, EEG, axis=0)
    return EEG_filtered

# 示例用法
fs = 1000  # 假设采样率为1000 Hz
t = np.linspace(0, 1, fs)  # 1秒的时间向量
EEG = np.sin(2 * np.pi * 10 * t) + 0.5 * np.random.randn(fs)  # 10 Hz正弦波加上一些噪声

# 高通滤波
EEG_highpass = highpass_filter(EEG, 0.5, fs)
# 低通滤波
EEG_lowpass = lowpass_filter(EEG, 35, fs)
# 带通滤波
EEG_bandpass = bandpass_filter(EEG, 0.5, 45, fs)
# 陷波滤波
EEG_notch = notch_filter(EEG, 49, 52, fs)

# 绘图
plt.figure(figsize=(12, 10))

plt.subplot(5, 1, 1)
plt.plot(t, EEG, label='Original EEG')
plt.title('Original EEG')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.subplot(5, 1, 2)
plt.plot(t, EEG_highpass, label='Highpass Filtered EEG (0.5 Hz)', color='r')
plt.title('Highpass Filtered EEG')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.subplot(5, 1, 3)
plt.plot(t, EEG_lowpass, label='Lowpass Filtered EEG (35 Hz)', color='g')
plt.title('Lowpass Filtered EEG')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.subplot(5, 1, 4)
plt.plot(t, EEG_bandpass, label='Bandpass Filtered EEG (0.5-45 Hz)', color='m')
plt.title('Bandpass Filtered EEG')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.subplot(5, 1, 5)
plt.plot(t, EEG_notch, label='Notch Filtered EEG (49-52 Hz)', color='c')
plt.title('Notch Filtered EEG')
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()

plt.tight_layout()
plt.show()
